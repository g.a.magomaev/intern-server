package store

type Config struct {
	DatabaseURL        string `toml:"database_url"`
	DatabaseDriverName string `toml:"database_driver_name"`
}

func NewConfig() *Config {
	return &Config{}
}
