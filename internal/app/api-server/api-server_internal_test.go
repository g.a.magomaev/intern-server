package api_server

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAPIServer_HandleUsers(t *testing.T) {
	s := New(NewConfig())
	res := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/users", nil)
	s.handleUsers().ServeHTTP(res, req)
	assert.Equal(t, res.Body.String(), "Users service")
}
