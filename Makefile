.PHONY: build
build:
	docker-compose up --build intern-server

.PHONY: run
run:
	docker-compose up intern-server

.PHONY: test
test:
	go test -v -race -timeout 30s ./...

#.DEFAULT_GOAL :=build