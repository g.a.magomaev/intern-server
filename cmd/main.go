package main

import (
	"flag"
	"github.com/BurntSushi/toml"
	"intern-server/internal/app/api-server"
	"log"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "configs/api-server.toml", "path to config file")
}

func main() {
	flag.Parse()

	config := api_server.NewConfig()
	_, err := toml.DecodeFile(configPath, config)
	if err != nil {
		log.Fatal(err)
	}
	server := api_server.New(config)
	if err := server.Start(); err != nil {
		log.Fatal(err)
	}
}
