FROM golang:1.17.5

RUN go version
ENV GOPATH=/

COPY ./ ./

# make wait-for-postgras.sh executable
RUN chmod +x wait-for-postgres.sh
# install psql
RUN apt-get update
RUN apt-get -y install postgresql-client
RUN apt-get install -y migrate

RUN go mod download
RUN go build -o intern-server ./cmd/main.go

EXPOSE 8000

CMD ["./intern-server","postgres","wait-for-postgres.sh"]


