module intern-server

go 1.13

require (
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/lib/pq v1.10.4 // indirect
	github.com/rubenv/sql-migrate v1.0.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/tomnomnom/anew v0.0.0-20210928131819-7faa0640750e // indirect
)
